<?php
/**
 * @file
 * Settings form, and the job creation form.
 *
 * @author Elvar M. Jensen
 */

/**
 * Return form for node filtering.
 */
function background_node_operations_form() {
  module_load_include('inc', 'node', 'node.admin');

  if (isset($form_state['values']['operation']) && $form_state['values']['operation'] == 'delete') {
    return node_multiple_delete_confirm($form_state, array_filter($form_state['values']['nodes']));
  }
  $form = node_filter_form();

  $form['#theme'] = 'node_filter_form';
  $form['admin']  = background_node_operations_node_admin_nodes();

  return $form;
}

/**
 * Validate background_node_operations_node_admin_nodes form submissions.
 *
 * Check if the current filter, contains any nodes.
 */
function background_node_operations_form_validate($form, &$form_state) {
  // Only perform validation when creating a job.
  if ($form_state['clicked_button']['#id'] != 'edit-create-job') {
    return TRUE;
  }

  $filter = node_build_filter_query();
  $count_query = db_query_slave(db_rewrite_sql('SELECT count(n.nid) FROM {node} n '. $filter['join'] .' INNER JOIN {users} u ON n.uid = u.uid '. $filter['where'] .' ORDER BY n.changed DESC'), $filter['args']);
  $count = db_result($count_query);

  if ($count == 0) {
    form_set_error('', t('No items selected.'));
  }

  if (!is_int(intval($form_state['values']['batch_count'])) || intval($form_state['values']['batch_count']) <= 0) {
    form_set_error('batch_count', t('Batch count can only be integer, and bigger than 0.'));
  }
}

/**
 * Form builder: Builds the node administration overview.
 */
function background_node_operations_node_admin_nodes() {
  $filter = node_build_filter_query();
  $result = pager_query(db_rewrite_sql('SELECT n.*, u.name FROM {node} n '. $filter['join'] .' INNER JOIN {users} u ON n.uid = u.uid '. $filter['where'] .' ORDER BY n.changed DESC'), 50, 0, NULL, $filter['args']);

  // Enable language column if locale is enabled or if we have any node with language
  $count = db_result(db_query_slave("SELECT COUNT(n.nid) FROM {node} n WHERE language != ''"));
  $multilanguage = (module_exists('locale') || $count);

  $form['options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Update options'),
  );
  $options = array();
  foreach (module_invoke_all('node_operations') as $operation => $array) {
    $options[$operation] = $array['label'];
  }

  $count_query = db_query_slave(db_rewrite_sql('SELECT count(n.nid) FROM {node} n '. $filter['join'] .' INNER JOIN {users} u ON n.uid = u.uid '. $filter['where'] .' ORDER BY n.changed DESC'), $filter['args']);
  $count = db_result($count_query);
  $form['options']['message'] = array(
    '#type' => 'markup',
    '#value' => sprintf('<p>This operation will update <span style="color: #c6574a; font-style: italic">%s</span> nodes.</p>', $count),
  );
  $form['options']['batch_count'] = array(
    '#type' => 'textfield',
    '#title' => t('Batch count'),
    '#description' => t('Number of nodes processed within each batch.'),
    '#default_value' => variable_get('batch_count', 150),
  );
  $form['options']['exclude_nodes'] = array(
    '#type' => 'textfield',
    '#title' => t('Exclude nodes from operation'),
    '#autocomplete_path' => 'background_node_operations/node/autocomplete',
  );
  $form['options']['operation'] = array(
    '#type' => 'select',
    '#options' => $options,
    '#default_value' => 'approve',
  );
  $form['options']['create_job'] = array(
    '#type' => 'submit',
    '#value' => t('Create background job'),
    '#submit' => array('background_node_operations_node_admin_nodes_submit'),
  );

  $languages = language_list();
  $destination = drupal_get_destination();
  $nodes = array();
  while ($node = db_fetch_object($result)) {
    $nodes[$node->nid] = '';
    $options = empty($node->language) ? array() : array('language' => $languages[$node->language]);
    $form['title'][$node->nid] = array('#value' => l($node->title, 'node/'. $node->nid, $options) .' '. theme('mark', node_mark($node->nid, $node->changed)));
    $form['name'][$node->nid] =  array('#value' => check_plain(node_get_types('name', $node)));
    $form['username'][$node->nid] = array('#value' => theme('username', $node));
    $form['status'][$node->nid] =  array('#value' => ($node->status ? t('published') : t('not published')));
    if ($multilanguage) {
      $form['language'][$node->nid] = array('#value' => empty($node->language) ? t('Language neutral') : t($languages[$node->language]->name));
    }
    $form['operations'][$node->nid] = array('#value' => l(t('edit'), 'node/'. $node->nid .'/edit', array('query' => $destination)));
  }

  $form['pager'] = array('#value' => theme('pager', NULL, 50, 0));
  $form['#theme'] = 'node_admin_nodes';
  return $form;
}

/**
 * Submit handler for background_node_operations_node_admin_nodes form.
 */
function background_node_operations_node_admin_nodes_submit($form, &$form_state) {
  $operations = module_invoke_all('node_operations');
  $operation = $operations[$form_state['values']['operation']];
  $filter = node_build_filter_query();
  $result = db_query_slave(db_rewrite_sql('SELECT n.nid FROM {node} n '. $filter['join'] .' INNER JOIN {users} u ON n.uid = u.uid '. $filter['where'] .' ORDER BY n.changed DESC'), $filter['args']);

  // Find all node id's.
  $nids = array();
  while($node = db_fetch_object($result)) {
    $nids[] = $node->nid;
  }

  // Remove excluded nodes.
  if (strlen($form_state['values']['exclude_nodes']) > 0) {
    $excluded_nodes = explode(',', $form_state['values']['exclude_nodes']);
    foreach ($excluded_nodes as $node) {
      $index = array_search($node, $nids);
      unset($nids[$index]);
    }
  }

  // Create and queue.
  _background_node_operations_queue_operation($nids, $operation, $form_state['values']['batch_count']);

  // Reset form filter.
  $_SESSION['node_overview_filter'] = array();

  drupal_set_message(t('Job is now queued, you can follow the status !link.', array(
    '!link' => l(t('here'), 'admin/content/node/background_node_operations/jobs'),
  )));
}

/**
 * Returns background operations settings system form.
 */
function background_node_operations_settings_form() {
  $form['batch_count'] = array(
    '#title' => 'Default batch count',
    '#type' => 'textfield',
    '#description' => t('Defines how many nodes is processed within each batch, this can also be altered on each job'),
    '#default_value' => variable_get('batch_count', 150),
  );
  $form['cron_frequency'] = array(
    '#title' => 'Cron frequency',
    '#type' => 'textfield',
    '#description' => t('How often cron is ran on your site (This doesn\'t specify how often cron runs). Jobs estimated time of arrival are calculated using this value.'),
    '#default_value' => variable_get('cron_frequency', 60),
  );

  return system_settings_form($form);
}
