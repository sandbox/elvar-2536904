<?php
/**
 * @file
 * Core functions for background_node_operations module.
 *
 * @author Elvar M. Jensen
 */

/**
 * Implements hook_menu().
 */
function background_node_operations_menu() {
  $items = array();

  $items['admin/content/node/background_node_operations'] = array(
    'title' => 'Background node operations',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('background_node_operations_form'),
    'access arguments' => array('administer nodes'),
    'file' => 'background_node_operations.forms.inc',
    'type' => MENU_LOCAL_TASK,
  );
  $items['admin/content/node/background_node_operations/new'] = array(
    'title' => 'Create background operation',
    'weight' => -1,
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );
  $items['admin/settings/background_node_operations'] = array(
    'title' => 'Background node operations settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('background_node_operations_settings_form'),
    'access arguments' => array('administer nodes'),
    'file' => 'background_node_operations.forms.inc',
  );
  $items['admin/content/node/background_node_operations/jobs'] = array(
    'title' => 'Operations overview',
    'page callback' => 'background_node_operations_overview',
    'access arguments' => array('administer nodes'),
    'type' => MENU_LOCAL_TASK,
  );
  $items['admin/content/node/background_node_operations/jobs/%status'] = array(
    'title' => 'Operations overview',
    'page arguments' => array(5),
    'access arguments' => array('administer nodes'),
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );
  $items['admin/content/node/background_node_operations/job/%/cancel'] = array(
    'page callback' => 'background_node_operations_cancel_job',
    'page arguments' => array(5),
    'access arguments' => array('administer nodes'),
    'type' => MENU_LOCAL_TASK,
  );
  $items['admin/content/node/background_node_operations/run'] = array(
    'page callback' => 'background_node_operations_run_batch',
    'page arguments' => array(5),
    'access arguments' => array('administer nodes'),
    'type' => MENU_LOCAL_TASK,
  );
  $items['background_node_operations/node/autocomplete'] = array(
    'title' => 'Autocomplete for nodes',
    'page callback' => '_background_node_operations_nodes_autocomplete',
    'access arguments' => array('administer nodes'),
    'type' => MENU_CALLBACK
  );

  return $items;
}

/**
 * Page callback, for canceling a job.
 */
function background_node_operations_cancel_job($jid) {
  // We don't really want to remove the job, for historical reasons.
  db_query_slave('UPDATE {background_node_operations_jobs} SET status="cancelled" WHERE id = %d', $jid);
  drupal_set_message(t('Job was successfully cancelled.'));
  drupal_goto('admin/content/node/background_node_operations/jobs');
}

/**
 * Page callback, for running a batch of the next job.
 */
function background_node_operations_run_batch() {
  background_node_operations_cron();
  drupal_set_message(t('Batch was successfully ran.'));
  drupal_goto('admin/content/node/background_node_operations/jobs');
}

/**
 * Page callback, for job overview.
 */
function background_node_operations_overview($status = 'waiting') {
  $cron_frequency = variable_get('cron_frequency', 60);

  $time_variable = $status == 'completed' ? 'completed' : 'created';
  $header = array(
    array('data' => t(ucfirst($time_variable)), 'field' => $time_variable . '_at', 'sort' => 'asc'),
    array('data' => t('Operation'), 'field' => 'config'),
    array('data' => t('Processed nodes'), 'field' => 'nodes_count_cache'),
    array('data' => t('ETA'), 'colspan' => 1),
    array('data' => t('Status'), 'field' => 'status'),
    array('data' => t('Actions'), 'colspan' => 1),
  );

  $res = pager_query("SELECT * FROM {background_node_operations_jobs} WHERE status IN ('%s', '%s')" . tablesort_sql($header),
    25,
    0,
    NULL,
    $status,
    $status == 'waiting' ? 'processing' : ''
  );

  $rows = array();
  $eta = strtotime("now");
  while ($row = db_fetch_object($res)) {
    $config = unserialize($row->config);

    // Calculate eta for this job.
    $batch_count = ceil(($row->nodes_count_cache - $row->processed_nodes) / $config['batch_count']);
    $expected_run_time = $batch_count * $cron_frequency;
    $eta = strtotime("+ " . $expected_run_time . " minutes", $eta);
    // Round to quarter hour.
    $rounded_eta = round($eta / (15 * 60)) * (15 * 60);

    $rows[] = array(
      $status == 'completed' ? format_date($row->completed_at) : format_date($row->created_at),
      $config['operation']['label'],
      $row->processed_nodes . "/" . $row->nodes_count_cache,
      $status == 'completed' ? '-' : format_date($rounded_eta),
      ucfirst($row->status),
      l('Remove', 'admin/content/node/background_node_operations/job/' . $row->id . '/cancel'),
    );
  }

  // Show links to completed and waiting list.
  $links = theme('item_list', array(
     l('Job queue', 'admin/content/node/background_node_operations/jobs'),
     l('Completed jobs', 'admin/content/node/background_node_operations/jobs/completed'),
     l('Manually run batch', 'admin/content/node/background_node_operations/run'),
    )
  );

  $html = $links . theme('table', $header, $rows) . theme('pager');

  return $html;
}

/**
 * Queues a bulk to the background.
 */
function _background_node_operations_queue_operation($node_ids, $operation, $batch_count = 150) {
  $config = array(
    'operation' => $operation,
    'batch_count' => $batch_count,
  );

  db_query_slave("INSERT INTO {background_node_operations_jobs} (created_at, nodes, nodes_count_cache, processed_nodes, status, config) VALUES (%d, '%s', %d, %d, '%s', '%s')",
    time(),
    implode(',', $node_ids),
    count($node_ids),
    0,
    'waiting',
    serialize($config)
  );
}

/**
 * Implements hook_cron().
 */
function background_node_operations_cron() {
  // Find current active job.
  $job = db_fetch_object(db_query_slave('SELECT * FROM {background_node_operations_jobs} WHERE status = "processing" LIMIT 1'));

  // If there wasn't a active job, pick the next.
  if (empty($job)) {
    $job = db_fetch_object(db_query_slave('SELECT * FROM {background_node_operations_jobs} WHERE status = "waiting" ORDER BY weight, created_at ASC LIMIT 1'));
  }

  // If a job was found, process a batch for that job.
  if (!empty($job)) {
    _background_node_operations_process_job_batch($job);
  }
}

/**
 * Processes a batch of a job.
 */
function _background_node_operations_process_job_batch($job) {
  $config = unserialize($job->config);
  $operation = $config['operation'];
  $processed_nodes = $job->processed_nodes;

  // Pick off where we left off.
  $nodes = explode(',', $job->nodes);
  $node_batch = array_slice($nodes, $processed_nodes, $config['batch_count']);

  // Run operation against batch of nodes.
  $function = $operation['callback'];
  // Add in callback arguments if present.
  if (isset($operation['callback arguments'])) {
    $args = array_merge(array($node_batch), $operation['callback arguments']);
  }
  else {
    $args = array($nodes);
  }
  // Override node_mass_update, which insist to run its own batches.
  if ($function == 'node_mass_update') {
    $function = '_background_node_operations_node_mass_update';
  }

  call_user_func_array($function, $args);

  // Reflect and update the job.
  // If all the nodes was processed, close the job.
  $processed_nodes += count($node_batch);
  if ($processed_nodes >= count($nodes)) {
    db_query_slave("UPDATE {background_node_operations_jobs} SET status='completed', processed_nodes=%d, completed_at=%d WHERE id = %d",
      $processed_nodes,
      time(),
      $job->id
    );
  }
  // Or update the progress of the job.
  else {
    db_query_slave("UPDATE {background_node_operations_jobs} SET status='processing', processed_nodes=%d WHERE id = %d",
      $processed_nodes,
      $job->id
    );
  }
}

/**
 * Node Mass Update - helper function.
 */
function _background_node_operations_node_mass_update($nids, $updates) {
  foreach ($nids as $nid) {
    $node = node_load($nid, NULL, TRUE);

    foreach ($updates as $name => $value) {
      $node->$name = $value;
    }
    node_save($node);
  }
}

/**
 * Node ajax auto-complete.
 */
function _background_node_operations_nodes_autocomplete($search) {
  // The user enters a comma-separated list of content types. We only autocomplete the last content type.
  $array = drupal_explode_tags($search);
  // Fetch last
  $last_string = trim(array_pop($array));

  $result = db_query_range("SELECT nid, title FROM {node} WHERE LOWER(title) LIKE LOWER('%s%')", $last_string, 0, 10);
  // Comma seperate each entry.
  $prefix = count($array) ? implode(', ', $array) .', ' : '';
  $matches = array();
  while ($node = db_fetch_object($result)) {
    $matches[$prefix . $node->nid] = check_plain($node->title);
  }

  print drupal_to_js($matches);
  exit();
}
